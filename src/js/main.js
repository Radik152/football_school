$(function(){
    $('.slider').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false
    });
});

$('.js__menu__toggle').on('click', function(){
    $(this).toggleClass('is-active');
    $('.header__menu').toggleClass('is__open');
});

$(function(){
    $('.why__we__wrapper').hide(0).show(1000);
})

// photogallery

let imgArray = ['images/photogallery/img1.png','images/photogallery/img2.jpg','images/photogallery/img3.jpg'];
let count = -1;

function rightArrow() {
    let img = document.getElementById('img__photogallery');
    if(count<imgArray.length-1 ) {
        count++;
    } else {
        count = 0;
    }
    img.src = imgArray[count];
}

function leftArrow() {
    let img = document.getElementById('img__photogallery');
    if(count>0 ) {
        count--;
    } else {
        count = imgArray.length-1;
    }
    img.src = imgArray[count];
}